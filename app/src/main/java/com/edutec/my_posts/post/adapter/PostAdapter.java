package com.edutec.my_posts.post.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.edutec.my_posts.R;
import com.edutec.my_posts.post.modelo.Post;

import java.util.ArrayList;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private Context context;
    private ArrayList<Post> posts;

    public PostAdapter(Context context, ArrayList<Post> posts) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.posts = posts;
    }

    /**
     * Inflo la vista del item en el recyclerview
     * @param parent
     * @param viewType
     * @return
     */
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_post, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    /**
     * Interactuar con el item seleccionado
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Post current = posts.get(position);
        holder.txtUsername.setText("Anonimo");
        holder.txtTextPost.setText(current.getTexto());

        holder.cardItem.setOnClickListener(e -> {
            Toast.makeText(context, current.getTexto(), Toast.LENGTH_SHORT).show();
        });
    }

    /**
     * Obtiene el tamaño del arraylist cargado en el recycerview
     * @return
     */
    @Override
    public int getItemCount() {
        return posts.size();
    }

    /**
     * Conectarnos con el item
     */
    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtUsername;
        TextView txtTextPost;
        CardView cardItem;

        public MyViewHolder(View itemView) {
            super(itemView);
            txtUsername = itemView.findViewById(R.id.txtUsername);
            txtTextPost = itemView.findViewById(R.id.txtTextPost);
            cardItem = itemView.findViewById(R.id.cardItem);
        }

    }

}
