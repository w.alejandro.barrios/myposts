package com.edutec.my_posts.post.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.edutec.my_posts.R;
import com.edutec.my_posts.post.adapter.PostAdapter;
import com.edutec.my_posts.post.modelo.Post;
import com.edutec.my_posts.user.activity.ProfilePictureActivity;
import com.edutec.my_posts.user.activity.SignInActivity;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.Date;

public class PostActivity extends AppCompatActivity {

    RecyclerView recyclerPosts;
    private ArrayList<Post> posts;
    private PostAdapter adapter;
    private Button btnOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        connect(); // Conexión con la vista

        posts = new ArrayList<>();
        Post post1 = new Post();
        post1.setIdPost(1);
        post1.setTexto("Hola mundo desde un post!");

        Post post2 = new Post();
        post2.setIdPost(1);
        post2.setTexto("Hola mundo desde un segundo post!");

        posts.add(post1);
        posts.add(post2);

        adapter = new PostAdapter(this, posts);
        recyclerPosts.setAdapter(adapter);

        btnOut.setOnClickListener(e -> {
            FirebaseAuth.getInstance().signOut();
            startActivity(new Intent(this, SignInActivity.class));
            finish();
        });

    }

    private void connect() {
        recyclerPosts = findViewById(R.id.recyclerPosts);
        recyclerPosts.setLayoutManager(new LinearLayoutManager(this));
        btnOut = findViewById(R.id.btnOut);
    }
}
