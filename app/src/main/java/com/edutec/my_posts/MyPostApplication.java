package com.edutec.my_posts;

import android.app.Application;

import com.edutec.my_posts.user.model.User;

public class MyPostApplication extends Application {

    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
