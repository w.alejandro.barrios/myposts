package com.edutec.my_posts;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.edutec.my_posts.post.activity.PostActivity;
import com.edutec.my_posts.user.activity.SignUpActivity;

import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends AppCompatActivity {
    private static final long SCREEN_DELAY = 2000;
    TextView txtSplash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        txtSplash = findViewById(R.id.txtSplash);

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                Intent intent = new Intent(getApplicationContext(), SignUpActivity.class);
                startActivity(intent);
                finish();
            }
        }, SCREEN_DELAY);

        // Recursividad
        int calculoFactorial = factorial(5);
        txtSplash.setText("" + calculoFactorial);
    }

    private int factorial(int n) {
        if(n == 0)
            return 1;
        else
            return n * factorial(n - 1);
    }

    // 5 * 24 = 120
}
