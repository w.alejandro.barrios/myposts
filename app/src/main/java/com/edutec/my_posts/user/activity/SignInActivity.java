package com.edutec.my_posts.user.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.edutec.my_posts.R;

public class SignInActivity extends AppCompatActivity {

    private TextView txtRegistro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        connect();

        txtRegistro.setOnClickListener(e -> {
            startActivity(new Intent(this, SignUpActivity.class));
            finish();
        });
    }

    private void connect() {
        txtRegistro = findViewById(R.id.txtRegistro);
    }
}

