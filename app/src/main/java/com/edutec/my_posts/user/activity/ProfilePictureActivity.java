package com.edutec.my_posts.user.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.edutec.my_posts.MyPostApplication;
import com.edutec.my_posts.R;
import com.edutec.my_posts.user.model.User;
import com.fxn.pix.Options;
import com.fxn.pix.Pix;
import com.fxn.utility.ImageQuality;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ProfilePictureActivity extends AppCompatActivity {

    private Button btnPicker;
    private ImageView imgProfile;
    private static final int REQUEST_CODE_IMAGE_PICKER = 5050;
    private FirebaseAuth auth;
    private FirebaseFirestore db;
    private FirebaseStorage storage = FirebaseStorage.getInstance();
    private StorageReference storageReference = storage.getReference();
    private FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_picture);
        connect();
        auth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();

        // PARA LEVANTAR EL IMAGE PICKER
        btnPicker.setOnClickListener(e -> {
            Options options = Options.init()
                    .setRequestCode(REQUEST_CODE_IMAGE_PICKER)
                    .setCount(1)
                    .setImageQuality(ImageQuality.HIGH)
                    .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)
                    .setPath("/my-posts/images/profile-pictures");

            Pix.start(this, options);
        });

        User user = ((MyPostApplication) this.getApplication()).getUser();
        // PARA DESCARGAR LA IMAGEN EJECUTANDO EL HILO ASÍNCRONO
        if (user.getProfilePicture() != null && !user.getProfilePicture().equals("")) {
            loadImage();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        user = auth.getCurrentUser();
    }

    /**
     *
     * @param requestCode
     * @param resultCode
     * @param data
     *
     * Todo lo que de un resultado en una actividad, termina aquí
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_IMAGE_PICKER  && resultCode == Activity.RESULT_OK) {

            // RECIBIMOS LISTADO DE IMAGENES
            ArrayList<String> pictures = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
            File imgFile = new File(pictures.get(0));

            // CONVERTIMOS A URI
            Uri uri = Uri.fromFile(imgFile);
            // REFERENCIA HACIA EL STORATE
            StorageReference profileReference = storageReference.child("profile-pictures/").child(uri.getLastPathSegment());
            UploadTask uploadTask = profileReference.putFile(uri);

            // SUBIDA DE ARHIVO
            uploadTask.addOnFailureListener(onFailure -> {
                Toast.makeText(this, "Error al subir imagen", Toast.LENGTH_SHORT).show();
            }).addOnSuccessListener(onSuccess -> {

                try {
                    storageReference.child(((MyPostApplication) this.getApplication()).getUser().getProfilePicture())
                            .delete();
                } catch (Exception e) {
                    Toast.makeText(this, "No hay imagen para eliminar", Toast.LENGTH_SHORT).show();
                }


                // ACTUALIZAR BASE DE DATOS
                Map<String, Object> dataFirebase = new HashMap<>();
                dataFirebase.put("profile-image", "profile-pictures/" + uri.getLastPathSegment());

                db.collection("user")
                        .document(user.getUid())
                        .update(dataFirebase)
                        .addOnSuccessListener(avoid -> {
                            Toast.makeText(this, "Subida exitosa", Toast.LENGTH_SHORT).show();
                            ((MyPostApplication) this.getApplication()).getUser().setProfilePicture("profile-pictures/"+uri.getLastPathSegment());
                            loadImage();
                        })
                        .addOnFailureListener(onFailed -> {
                            Toast.makeText(this, "Almacenamiento erroneo", Toast.LENGTH_SHORT).show();
                        });

            });

        }
    }

    private void loadImage() {
        User user = ((MyPostApplication) this.getApplication()).getUser();
        storageReference.child(user.getProfilePicture())
                .getDownloadUrl()
                .addOnSuccessListener(uri -> {
                    //new DownloadImageThread().execute(uri.toString()); // Ejecucion del hilo
                    Picasso.get().load(uri.toString()).into(imgProfile);
                }).addOnFailureListener(onFailure -> {
            Toast.makeText(this, "Error al cargar imagen", Toast.LENGTH_SHORT).show();
        });
    }

    private void connect() {
        btnPicker = findViewById(R.id.btnPick);
        imgProfile = findViewById(R.id.imgProfile);
    }

    private class DownloadImageThread extends AsyncTask<String, Void, Bitmap> {

        // SE EJECUTA ANTES
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Toast.makeText(getApplicationContext(), "Cargando Imagen...", Toast.LENGTH_SHORT).show();
        }

        // ES LA EJECUCIÓN EN BACKGROUND
        @Override
        protected Bitmap doInBackground(String... urlImage) {

            String imageUrl = urlImage[0];
            Bitmap bitmap = null;

            try {
                InputStream inputStream = new URL(imageUrl).openStream();
                bitmap = BitmapFactory.decodeStream(inputStream);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return bitmap;
        }

        // SE EJECUTA DESPUÉS
        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            imgProfile.setImageBitmap(bitmap);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
