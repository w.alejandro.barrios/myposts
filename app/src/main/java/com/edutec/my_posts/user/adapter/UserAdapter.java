package com.edutec.my_posts.user.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.edutec.my_posts.R;
import com.edutec.my_posts.user.activity.UsersActivity;
import com.edutec.my_posts.user.fragment.DetailsFragment;
import com.edutec.my_posts.user.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.MyViewHolder> {

    private FirebaseFirestore db;
    private LayoutInflater inflater;
    private Context context;
    private ArrayList<User> users;
    private FirebaseStorage storage = FirebaseStorage.getInstance();
    private StorageReference storageReference = storage.getReference();

    public UserAdapter(Context context, ArrayList<User> users) {
        db = FirebaseFirestore.getInstance();
        this.context = context;
        this.users = users;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_user, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        User current = users.get(position);

        holder.txtUsername.setText(current.getUsername());
        holder.txtName.setText(current.getName());
        holder.txtEmail.setText(current.getEmail());

        // CARGA DE IMAGENES EN RECYCLERVIEW
        storageReference.child(current.getProfilePicture())
                .getDownloadUrl()
                .addOnSuccessListener(uri -> {
                    //new DownloadImageThread().execute(uri.toString()); // Ejecucion del hilo
                    Picasso.get().load(uri.toString()).into(holder.imgProfilePicture);
                }).addOnFailureListener(onFailure -> {
            Toast.makeText(context, "Error al cargar imagen", Toast.LENGTH_SHORT).show();
        });

        holder.cardItem.setOnClickListener(e -> {
            Fragment detailsFragment = new DetailsFragment(current.getId());
            UsersActivity usersActivity = (UsersActivity) context;
            usersActivity.frameLayout.setVisibility(View.VISIBLE);
            FragmentManager fragmentManager = usersActivity.getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            String TAG = DetailsFragment.class.getName();
            fragmentTransaction.addToBackStack(TAG);
            fragmentTransaction.replace(R.id.fragmentContainer, detailsFragment);
            fragmentTransaction.commit();
        });

        holder.cardItem.setOnLongClickListener(e -> {
            db.collection("user")
                    .document(current.getId())
                    .delete()
                    .addOnSuccessListener(onSuccess -> {
                        Toast.makeText(context, "Eliminado exitosamente", Toast.LENGTH_SHORT).show();
                        users.remove(current);
                        notifyDataSetChanged();
                    })
                    .addOnFailureListener(onFailure -> {
                        Toast.makeText(context, "Eliminacion fallida", Toast.LENGTH_SHORT).show();
                    });
            return true;
        });
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtUsername;
        TextView txtEmail;
        TextView txtName;
        CardView cardItem;
        ImageView imgProfilePicture;

        public MyViewHolder(View itemView) {
            super(itemView);
            txtUsername = itemView.findViewById(R.id.txtUsername);
            txtEmail = itemView.findViewById(R.id.txtEmail);
            txtName = itemView.findViewById(R.id.txtName);
            cardItem = itemView.findViewById(R.id.cardItem);
            imgProfilePicture = itemView.findViewById(R.id.imgProfilePicture);
        }

    }
}
