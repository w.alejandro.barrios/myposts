package com.edutec.my_posts.user.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.edutec.my_posts.MyPostApplication;
import com.edutec.my_posts.R;
import com.edutec.my_posts.post.activity.PostActivity;
import com.edutec.my_posts.user.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class SignUpActivity extends AppCompatActivity {

    private FirebaseAuth auth;
    private EditText edtPassword, edtEmail, edtName, edtUsername;
    private Button btnSignUp;
    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        connect();
        auth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();

        btnSignUp.setOnClickListener(e -> {
            String email, password;
            email = edtEmail.getText().toString();
            password = edtPassword.getText().toString();
            signUpWithEmailAndPassword(email, password);
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = auth.getCurrentUser();
        if (currentUser != null) {
            saveUser(currentUser.getUid());
            startActivity(new Intent(this, UsersActivity.class));
            finish();
        }
    }

    private void signUpWithEmailAndPassword(String email, String password) {
        auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, onComplete -> {
                    FirebaseUser currentUser;
                    if (onComplete.isSuccessful()) {
                        currentUser = auth.getCurrentUser();
                        createAccount(currentUser);
                        startActivity(new Intent(this, UsersActivity.class));
                        finish();
                    } else {
                        currentUser = null;
                        Toast.makeText(this, "Error al crear la cuenta", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void createAccount(FirebaseUser firebaseUser) {
        if (firebaseUser != null) {
            Map<String, Object> data = new HashMap<>();

            User user = new User();
            user.setName(edtName.getText().toString());
            user.setUsername(edtUsername.getText().toString());
            user.setEmail(firebaseUser.getEmail());
            user.setId(firebaseUser.getUid());

            data.put("name", user.getName());
            data.put("username", user.getUsername());
            data.put("email", user.getEmail());
            data.put("id", user.getId());

            //Timestamp timestamp = Timestamp.now();
            //Long tmpId = timestamp.getSeconds();
            db.collection("user")
                    .document(user.getId())
                    .set(data)
                    .addOnSuccessListener(avoid -> {
                        Toast.makeText(this, "Exitoso", Toast.LENGTH_SHORT).show();
                        saveUser(user.getId());
                    })
                    .addOnFailureListener(onFailure -> {
                        Toast.makeText(this, "Error al almacenar", Toast.LENGTH_SHORT).show();
                    });

        } else
            Toast.makeText(this, "Error al crear la cuenta", Toast.LENGTH_SHORT).show();
    }

    private void saveUser(String currentUser) {
        db.collection("user")
                .document(currentUser)
                .get()
                .addOnSuccessListener(onSuccess -> {
                    User user = new User();
                    Map<String, Object> data = onSuccess.getData();
                    user.setUsername(data.get("username").toString());
                    user.setEmail(data.get("email").toString());
                    user.setName(data.get("name").toString());
                    user.setId(data.get("id").toString());
                    if (data.containsKey("profile-image"))
                        user.setProfilePicture(data.get("profile-image").toString());
                    ((MyPostApplication) this.getApplication()).setUser(user);
                });
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, SignInActivity.class));
        finish();
    }

    private void connect() {
        edtEmail = findViewById(R.id.edtEmail);
        edtPassword = findViewById(R.id.edtPassword);
        btnSignUp = findViewById(R.id.btnSignUp);
        edtName = findViewById(R.id.edtName);
        edtUsername = findViewById(R.id.edtUsername);
    }
}
