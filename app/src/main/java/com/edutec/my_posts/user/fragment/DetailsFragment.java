package com.edutec.my_posts.user.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.edutec.my_posts.MyPostApplication;
import com.edutec.my_posts.R;
import com.edutec.my_posts.user.model.User;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.Map;

public class DetailsFragment extends Fragment {

    private String userUid;
    private TextView txtName, txtEmail, txtUsername;
    private ImageView imgProfile;
    private User detailUser;
    private FirebaseFirestore db;
    private FirebaseStorage storage = FirebaseStorage.getInstance();
    private StorageReference storageReference = storage.getReference();

    public DetailsFragment(String userUid) {
        this.userUid = userUid;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_details, container, false);

        db = FirebaseFirestore.getInstance();

        txtName = rootView.findViewById(R.id.txtName);
        txtEmail = rootView.findViewById(R.id.txtEmail);
        txtUsername = rootView.findViewById(R.id.txtUsername);
        imgProfile = rootView.findViewById(R.id.imgProfile);

        detailUser = new User();
        db.collection("user")
                .document(userUid)
                .get()
                .addOnSuccessListener(onSuccess -> {

                    Map<String, Object> data = onSuccess.getData();
                    detailUser.setId(data.get("id").toString());
                    detailUser.setName(data.get("name").toString());
                    detailUser.setEmail(data.get("email").toString());
                    detailUser.setUsername(data.get("username").toString());
                    if (data.containsKey("profile-image"))
                        detailUser.setProfilePicture(data.get("profile-image").toString());

                    loadProfile();

                }).addOnFailureListener(onFailure -> {
            Toast.makeText(getActivity(), "No se ha encontrado el usuario", Toast.LENGTH_SHORT).show();
        });

        // CONSUMIR APPLICATION DESDE FRAGMENT
        User usuarioApplication = ((MyPostApplication) getActivity().getApplication()).getUser();
        Toast.makeText(getActivity(), usuarioApplication.getName(), Toast.LENGTH_SHORT).show();

        return rootView;
    }

    private void loadProfile() {
        txtName.setText(detailUser.getName());
        txtEmail.setText(detailUser.getEmail());
        txtUsername.setText(detailUser.getUsername());

        storageReference.child(detailUser.getProfilePicture())
                .getDownloadUrl()
                .addOnSuccessListener(uri -> {
                    //new DownloadImageThread().execute(uri.toString()); // Ejecucion del hilo
                    Picasso.get().load(uri.toString()).into(imgProfile);
                }).addOnFailureListener(onFailure -> {
            Toast.makeText(getActivity(), "Error al cargar imagen", Toast.LENGTH_SHORT).show();
        });
    }

}
