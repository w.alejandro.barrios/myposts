package com.edutec.my_posts.user.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.edutec.my_posts.R;
import com.edutec.my_posts.user.adapter.UserAdapter;
import com.edutec.my_posts.user.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class UsersActivity extends AppCompatActivity {

    RecyclerView recyclerUsers;
    private UserAdapter adapter;
    private ArrayList<User> users;
    private FirebaseFirestore db;
    private FirebaseAuth auth;
    private FirebaseUser fUser;
    private Button btnOut, btnImg;
    private static final int REQUEST_CODE_PERMISSION = 456;
    public FrameLayout frameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);
        connect();
        auth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();

        // SOLICITAMOS PERMISOS
        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
        || (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(this, new String[] {
                    Manifest.permission.CAMERA,
                    Manifest.permission.READ_EXTERNAL_STORAGE
            }, REQUEST_CODE_PERMISSION);
        }

        users = new ArrayList<>(); // Comentario para commit

        db.collection("user")
                .get()
                .addOnCompleteListener(onComplete -> {
                    if (onComplete.isSuccessful()) {
                        for (QueryDocumentSnapshot document : onComplete.getResult()) {
                            User user = new User();
                            user.setId(document.getId());
                            user.setName(document.getData().get("name").toString());
                            user.setEmail(document.getData().get("email").toString());
                            user.setUsername(document.getData().get("username").toString());
                            if (document.getData().containsKey("profile-image"))
                                user.setProfilePicture(document.getData().get("profile-image").toString());
                            users.add(user);
                        }
                        adapter = new UserAdapter(this, users);
                        recyclerUsers.setAdapter(adapter);
                    } else {
                        Toast.makeText(this, "Error al leer los documentos", Toast.LENGTH_SHORT).show();
                    }
                });


        btnOut.setOnClickListener(e -> {
            FirebaseAuth.getInstance().signOut();
            startActivity(new Intent(this, SignInActivity.class));
            finish();
        });
        /*
        FOREACH
        for (User user : users) {

        } */

        btnImg.setOnClickListener(e -> {
            startActivity(new Intent(this, ProfilePictureActivity.class));
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        fUser = auth.getCurrentUser();
        /* LEER DATOS DEL USUARIO ACTUAL
        db.collection("user")
                .document(fUser.getUid())
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                    }
                }) */
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        frameLayout.setVisibility(View.GONE);
    }

    private void connect() {
        recyclerUsers = findViewById(R.id.recyclerUsers);
        recyclerUsers.setLayoutManager(new LinearLayoutManager(this));
        btnOut = findViewById(R.id.btnOut);
        btnImg = findViewById(R.id.btnImg);
        frameLayout = findViewById(R.id.fragmentContainer);
    }
}
